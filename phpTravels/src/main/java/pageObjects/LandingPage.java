package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;





public class LandingPage {
	
	
	public WebDriver driver;
	By expedia_checkDemo=By.cssSelector("a[href*='https://www.phptravels.net/public/expedia/']");
	
	public LandingPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
	}
	
	public WebElement getExpedia()
	{
		return driver.findElement(expedia_checkDemo);
	}


}

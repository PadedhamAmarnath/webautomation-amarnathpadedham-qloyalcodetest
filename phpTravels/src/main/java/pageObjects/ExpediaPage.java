package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;





public class ExpediaPage {
	
	
	public WebDriver driver;
	By expedia_input=By.id("citiesInput");
	By expedia_checkin=By.name("checkin");
	By expedia_checkout=By.name("checkout");
	By expedia_search=By.xpath("//*[@id='properties']/form/div[5]/button");
	
	public ExpediaPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
	}
	
	public WebElement getExpediaInput()
	{
		return driver.findElement(expedia_input);
	}
	public WebElement getExpediaCheckin()
	{
		return driver.findElement(expedia_checkin);
	}
	public WebElement getExpediaCheckout()
	{
		return driver.findElement(expedia_checkout);
	}
	public WebElement getExpediaSearch()
	{
		return driver.findElement(expedia_search);
	}




}

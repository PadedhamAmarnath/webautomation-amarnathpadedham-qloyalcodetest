

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pageObjects.ExpediaPage;
import pageObjects.ExpediaResults;
import pageObjects.LandingPage;
import resources.base;

public class SmokeTest extends base {
	
	public static Logger log=LogManager.getLogger(base.class.getName());
	
	
	@BeforeTest
	public void initialize() throws IOException
	{
		
		driver=initializeDriver();
		log.info("Driver is initialized");
		
	}
	
	@Test
	public void basePageNavigation() throws IOException
	{
		
		
		
		driver.get(prop.getProperty("url"));
		driver.manage().window().maximize();
	    log.info("Navigated to Expedia Screen");
	    ExpediaPage e=new ExpediaPage(driver);
	    e.getExpediaInput().sendKeys("Sydney");
	    DateFormat f = new SimpleDateFormat("MM/dd/yyyy");
	    Date date1 = new Date();
	    String checkin=f.format(date1);
	    GregorianCalendar cal=new GregorianCalendar();
	    cal.setTime(date1);
	    cal.add(Calendar.DATE,2);
	    Date date2=cal.getTime();
	    String checkout=f.format(date2);
	    System.out.println(checkin);
	    //Entering today's date instead of hard-coded date
	    e.getExpediaCheckin().sendKeys(checkin);
	    //Entering Future date instead of hard-coded date
	    e.getExpediaCheckout().sendKeys(checkout);
	    e.getExpediaSearch().click();
	    for(String childWindow: driver.getWindowHandles())
	    {
	    	//switch to child window
	    	driver.switchTo().window(childWindow);
	    }
	    
	    log.info("Navigated to Booking Screen");
	    
	    ExpediaResults er=new ExpediaResults(driver);
	    er.getDetails().click();
	    for(String childWindow: driver.getWindowHandles())
	    {
	    	//switch to child window
	    	driver.switchTo().window(childWindow);
	    }
	    
	    JavascriptExecutor js=(JavascriptExecutor) driver;
	    js.executeScript("window.scrollBy(0,1000)");
	    er.getBookNow().click();
	    for(String childWindow: driver.getWindowHandles())
	    {
	    	//switch to child window
	    	driver.switchTo().window(childWindow);
	    }
	    er.getBookGuest().click();   
	    log.info("Successfully navigated to Guest Booking Screen ");
	    
	    
	    
	}

	
	
	@AfterTest
	public void teardown()
	{
		//dispose method which closes all the browser windows and ends the WebDriver session gracefully
		//driver.quit();
		//driver.close();
	}

}

package pageObjects;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;





public class ExpediaResults {
	
	
	public WebDriver driver;
	By Details1=By.xpath("//*[@id='body-section']/div[5]/div/div[3]/div/table/tbody/tr[1]/td/div[3]/a/button");
	By BookNow=By.xpath("//*[@id=\"ROOMS\"]/form[1]/div/div[3]/div[2]/span");
	By BookAsGuest=By.xpath("//*[contains(text(),'Book as a Guest')]");
	//By Details1=By.xpath("//*[@class='btn btn-primary loader loader btn-block']//*text()='Details'");
	

	
	public ExpediaResults(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
	}
	
	public WebElement getDetails()
	{
		return driver.findElement(Details1);
	}
	public WebElement getBookNow()
	{
		return driver.findElement(BookNow);
	}
	public WebElement getBookGuest()
	{
		return driver.findElement(BookAsGuest);
}
}


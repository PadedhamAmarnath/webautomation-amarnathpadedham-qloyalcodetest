
# Instructions for Execution of Web Automation Project - Role Applied is for Automation Engineer

# Framework
This REST API Test is Automated using SElenium WebDriver on Eclipse IDE usig Maven Project, TestNG Framework.

#Libraries/Files Uploaded to the branch
1.All the source files are located in Maven project phpTravels,including browser driver exe files.


#Design details
1. This SmokeTest Automation is not complete yet, as I see that the application behaviour is not stable and Pages doesn't load consistently.
2. So far I have created couple of PageObjectModel Pages
   a. LandingPage.java
   b. Expedia Page.java
3. Created resource files
   a. data.properties - This properties file is used to store global variables. Here browser type and url are saved in this file.
   a. base.java - This class initializes Driver and sets Driver to Browser type passed through resource file data.properties.
4. Created one Test Class file SmokeTest.java 
   This class executes the Smoke Tests, launches the browser & navigates to url by reading the values from global properties file.
   Navigates to the Expedia Page, selects Ciy,Checkin, Checkout dates and clicks on Search button.

#Pending
1. Afte the Search Results are populated what is the criteria for selection from the results displayed to complete booking.
2. Other booking links like Ex: HotelBeds Hotel Module is displaying 404 Page Not Found error.
3. Amadues Flight Module is dislaying Error: Invalid Date Received.